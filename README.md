A mod that makes it so you can set a radio station as "default"; basically the radio station will automatically be tuned to when you enter a vehicle.

Several other mods exist that do this, but the main differences between this mod and the others are:

* This mod can set the default station to any station, including "off" and Self Radio.
* This mod can be disabled while in a mission.


Installation:

* Install Script Hook V & Script Hook V .NET.
* Download DefaultRadio.zip from the Releases section.
* Extract the DefaultRadio.dll and DefaultRadio.ini files to the scripts folder in your GTA V directory.

Usage:

* When in-game, select a radio station like normal, then press Control + K to save the station.
* Press Control + F9 to toggle the mod entirely.
* Press Control + F10 to toggle the disabling of the mod when in a mission.

Changeable in the INI file:

* The default radio station (easier to change this in-game).
* Whether to disable in missions or not.
* All keys. (Use https://msdn.microsoft.com/en-us/library/system.windows.forms.keys%28v=vs.110%29.aspx to find key names. Separate multiple keys with a + sign.)

Credits:

* /u/AppleShampoo102 from Reddit for the request.
* Jitnaught for writing the script.