﻿using GTA;
using GTA.Native;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DefaultRadio
{
    public class DefaultRadio : Script
    {
        private string[] RADIO_STATIONS = new string[]
        {
            "RADIO_01_CLASS_ROCK",
            "RADIO_02_POP",
            "RADIO_03_HIPHOP_NEW",
            "RADIO_04_PUNK",
            "RADIO_05_TALK_01",
            "RADIO_06_COUNTRY",
            "RADIO_07_DANCE_01",
            "RADIO_08_MEXICAN",
            "RADIO_09_HIPHOP_OLD",
            "RADIO_11_TALK_02",
            "RADIO_12_REGGAE",
            "RADIO_13_JAZZ",
            "RADIO_14_DANCE_02",
            "RADIO_15_MOTOWN",
            "RADIO_16_SILVERLAKE",
            "RADIO_17_FUNK",
            "RADIO_18_90S_ROCK",
            "RADIO_19_USER",
            "RADIO_20_THELAB",
            "HIDDEN_RADIO_AMBIENT_TV_BRIGHT",
            "OFF"
        };

        private string[] RADIO_STATION_DISPLAY_NAMES = new string[]
        {
            "Los Santos Rock Radio",
            "Non-Stop-Pop FM",
            "Radio Los Santos",
            "Channel X",
            "West Coast Talk Radio",
            "Rebel Radio",
            "Soulwax FM",
            "East Los FM",
            "West Coast Classics",
            "RADIO_11_TALK_02",
            "Blue Ark",
            "Worldwide FM",
            "FlyLo FM",
            "The Lowdown",
            "Radio Mirror Park",
            "Space",
            "Vinewood Boulevard Radio",
            "Self Radio",
            "The Lab",
            "HIDDEN_RADIO_AMBIENT_TV_BRIGHT",
            "Off"
        };

        private bool enabled = true;
        private bool alreadySetStation = false, wasInMission = false;

        public DefaultRadio()
        {
            INI.Initialize(Settings);

            bool savedRadioStationCorrect = !string.IsNullOrWhiteSpace(INI.Settings.savedRadioStation) && RADIO_STATIONS.Contains(INI.Settings.savedRadioStation);

            if (INI.Settings.saveRadioStationKeys != null || savedRadioStationCorrect)
            {
                Mission.Initialize();

                Interval = 100;
                Tick += DefaultRadio_Tick;

                if (INI.Settings.saveRadioStationKeys != null || INI.Settings.toggleDisableInMissionsKeys != null) KeyDown += DefaultRadio_KeyDown;
            }
            else
            {
                UI.ShowSubtitle("DefaultRadio not enabled due to no keys or station set");
            }
        }

        private void DefaultRadio_KeyDown(object sender, KeyEventArgs e)
        {
            if (INI.Settings.toggleModKeys != null && ((INI.Settings.toggleModKeys.Length == 1 && e.KeyCode == INI.Settings.toggleModKeys[0])
                || INI.Settings.toggleModKeys.All(x => Game.IsKeyPressed(x))))
            {
                enabled = !enabled;

                UI.ShowSubtitle("DefaultRadio " + (enabled ? "enabled" : "disabled"));
            }
            else if (enabled)
            {
                if (INI.Settings.saveRadioStationKeys != null && ((INI.Settings.saveRadioStationKeys.Length == 1
                    && e.KeyCode == INI.Settings.saveRadioStationKeys[0]) || INI.Settings.saveRadioStationKeys.All(x => x == e.KeyCode || Game.IsKeyPressed(x))))
                {
                    string currentRadioStation = Function.Call<string>(Hash.GET_PLAYER_RADIO_STATION_NAME);

                    if (currentRadioStation == "") currentRadioStation = RADIO_STATIONS[20]; //off

                    INI.Settings.savedRadioStation = currentRadioStation;
                    INI.Settings.Save();

                    UI.ShowSubtitle("Default radio station is now " + RADIO_STATION_DISPLAY_NAMES[Array.IndexOf(RADIO_STATIONS, currentRadioStation)]);
                }

                if (INI.Settings.toggleDisableInMissionsKeys != null && ((INI.Settings.toggleDisableInMissionsKeys.Length == 1
                    && e.KeyCode == INI.Settings.toggleDisableInMissionsKeys[0]) || INI.Settings.toggleDisableInMissionsKeys.All(x => x == e.KeyCode || Game.IsKeyPressed(x))))
                {
                    INI.Settings.disableInMissions = !INI.Settings.disableInMissions;
                    INI.Settings.Save();

                    UI.ShowSubtitle("Default radio station is now " + (INI.Settings.disableInMissions ? "disabled" : "enabled") + " in missions");
                }
            }
        }

        private void DefaultRadio_Tick(object sender, EventArgs e)
        {
            if (enabled)
            {
                if (Game.MissionFlag)
                    if (!wasInMission) wasInMission = true;
                    else if (wasInMission && Mission.DidMissionEnd)
                        wasInMission = false;

                if (!wasInMission || !INI.Settings.disableInMissions)
                {
                    Ped plrPed = Game.Player.Character;

                    if (plrPed.IsInVehicle())
                    {
                        if (!alreadySetStation)
                        {
                            Vehicle veh = plrPed.CurrentVehicle;

                            if (veh != null && veh.Exists() && veh.IsAlive && veh.EngineRunning && (plrPed.SeatIndex == VehicleSeat.Driver || !Function.Call<bool>(Hash.GET_IS_TASK_ACTIVE, plrPed, 165)))
                            {
                                alreadySetStation = true;

                                int tries = 0;

                                while (tries < 5)
                                {
                                    Function.Call(Hash.SET_RADIO_TO_STATION_NAME, INI.Settings.savedRadioStation);
                                    tries++;
                                    Wait(100);
                                }
                                
                                //debug messages{
                                //if (Function.Call<string>(Hash.GET_PLAYER_RADIO_STATION_NAME) == INI.Settings.savedRadioStation) UI.ShowSubtitle("Set radio station to " + INI.Settings.savedRadioStation);
                                //else UI.ShowSubtitle("Was not able to set radio station to " + INI.Settings.savedRadioStation);
                                //}
                            }
                        }
                    }
                    else alreadySetStation = false;
                }
            }
        }
    }
}
