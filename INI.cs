﻿using GTA;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DefaultRadio
{
    class INI
    {
        public class INISettings
        {
            private const string INI_SECTION_SETTINGS = "SETTINGS", INI_SECTION_KEYS = "KEYS";

            private const string INI_KEY_SAVED_RADIO_STATION = "SAVED_RADIO_STATION",
                INI_KEY_DISABLE_IN_MISSIONS = "DISABLE_IN_MISSIONS",
                INI_KEY_TOGGLE_MOD_KEYS = "TOGGLE_MOD_KEYS",
                INI_KEY_SAVE_RADIO_STATION_KEYS = "SAVE_RADIO_STATION_KEYS",
                INI_KEY_TOGGLE_DISABLE_IN_MISSIONS_KEYS = "TOGGLE_DISABLE_IN_MISSIONS_KEYS";

            public string savedRadioStation;

            public bool disableInMissions;

            public Keys[] toggleModKeys, saveRadioStationKeys, toggleDisableInMissionsKeys;

            private Keys[] parseKeys(string strKeys)
            {
                if (!string.IsNullOrWhiteSpace(strKeys))
                {
                    string[] sArrKeys = strKeys.Split('+');

                    List<Keys> keys = new List<Keys>();

                    foreach (string strKey in sArrKeys)
                    {
                        if (!string.IsNullOrWhiteSpace(strKey))
                        {
                            Keys key;

                            if (Enum.TryParse(strKey.Trim(), out key)) keys.Add(key);
                        }
                    }

                    if (keys.Count == sArrKeys.Length) return keys.ToArray();
                }

                return null;
            }

            private ScriptSettings _scriptSettings;

            internal INISettings(ScriptSettings scriptSettings)
            {
                _scriptSettings = scriptSettings;

                savedRadioStation = scriptSettings.GetValue(INI_SECTION_SETTINGS, INI_KEY_SAVED_RADIO_STATION, "").ToUpper();
                disableInMissions = scriptSettings.GetValue(INI_SECTION_SETTINGS, INI_KEY_DISABLE_IN_MISSIONS, true);

                toggleModKeys = parseKeys(scriptSettings.GetValue(INI_SECTION_KEYS, INI_KEY_TOGGLE_MOD_KEYS, ""));
                saveRadioStationKeys = parseKeys(scriptSettings.GetValue(INI_SECTION_KEYS, INI_KEY_SAVE_RADIO_STATION_KEYS, ""));
                toggleDisableInMissionsKeys = parseKeys(scriptSettings.GetValue(INI_SECTION_KEYS, INI_KEY_TOGGLE_DISABLE_IN_MISSIONS_KEYS, ""));
            }

            public void Save()
            {
                _scriptSettings.SetValue(INI_SECTION_SETTINGS, INI_KEY_SAVED_RADIO_STATION, savedRadioStation);
                _scriptSettings.SetValue(INI_SECTION_SETTINGS, INI_KEY_DISABLE_IN_MISSIONS, disableInMissions);
                _scriptSettings.Save();
            }
        }

        public static INISettings Settings;

        public static void Initialize(ScriptSettings scriptSettings)
        {
            Settings = new INISettings(scriptSettings);
        }
    }
}
