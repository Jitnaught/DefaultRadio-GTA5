﻿using GTA.Native;

namespace DefaultRadio
{
    class Mission
    {
        private class MissionData
        {
            public int numMissionsCompleted;
            public int sp0MissionsFailed, sp1MissionsFailed, sp2MissionsFailed;
            public int timesMissionSkipped;

            //num_missions_completed
            //sp0_missions_failed, sp1_missions_failed, sp2_missions_failed
            //times_mission_skipped
            public MissionData()
            {
                OutputArgument outArg = new OutputArgument();

                if (Function.Call<bool>(Hash.STAT_GET_INT, "num_missions_completed", outArg, -1))
                {
                    numMissionsCompleted = outArg.GetResult<int>();
                }
                else numMissionsCompleted = 0;

                if (Function.Call<bool>(Hash.STAT_GET_INT, "sp0_missions_failed", outArg, -1))
                {
                    sp0MissionsFailed = outArg.GetResult<int>();
                }
                else sp0MissionsFailed = 0;

                if (Function.Call<bool>(Hash.STAT_GET_INT, "sp1_missions_failed", outArg, -1))
                {
                    sp1MissionsFailed = outArg.GetResult<int>();
                }
                else sp1MissionsFailed = 0;

                if (Function.Call<bool>(Hash.STAT_GET_INT, "sp2_missions_failed", outArg, -1))
                {
                    sp2MissionsFailed = outArg.GetResult<int>();
                }
                else sp2MissionsFailed = 0;

                if (Function.Call<bool>(Hash.STAT_GET_INT, "times_mission_skipped", outArg, -1))
                {
                    timesMissionSkipped = outArg.GetResult<int>();
                }
                else timesMissionSkipped = 0;
            }

            public override bool Equals(object obj)
            {
                if (obj == null) return false;

                var data = obj as MissionData;

                return (numMissionsCompleted == data.numMissionsCompleted) && 
                    (sp0MissionsFailed == data.sp0MissionsFailed) && (sp1MissionsFailed == data.sp1MissionsFailed) && 
                    (sp2MissionsFailed == data.sp2MissionsFailed) && (timesMissionSkipped == data.timesMissionSkipped);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        private static MissionData lastMissionData;

        public static bool DidMissionEnd
        {
            get
            {
                var newMissionData = new MissionData();

                bool equal = lastMissionData.Equals(newMissionData);

                lastMissionData = newMissionData;

                return equal;
            }
        }

        public static void Initialize()
        {
            lastMissionData = new MissionData();
        }
    }
}
